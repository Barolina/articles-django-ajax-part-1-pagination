from django import template
register = template.Library()
@register.assignment_tag
def get_pageloader(page_url, *args, **kwargs):
    
    pagediv_id = kwargs.get("pagediv_id", "#pagediv-id")
    loadbutton_id = kwargs.get("loadbutton_id", "#load-id")
    pagination_id = kwargs.get("pagination_id", "#pagination-id")
    
    button_script = """
                        $("#{loadbutton_id}").click(function(){
                            load_page(
                                '{page_url},
                                '{pagination_id},
                                '{loadbutton_id},
                                '{pagediv_id},
                              );
                        });
                     """.format(
                                page_url = page_url,
                                pagediv_id = page_div_id,
                                pagination_id = pagination_id,
                                loadbutton_id = loadbutton_id,
                        )
    
    return {
                "pagediv": "<div id='" + pagediv_id + "'></div>",
                "loadbutton": "<button id='" + loadbutton_id
                                + "' type='submit'>Show</button>",
                "pagination": "<input type='hidden' id='"
                                + pagination_id + "' value='1'>",
                "button_script": button_script,
            }
