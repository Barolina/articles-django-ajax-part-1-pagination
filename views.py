from django.views.generic.list import ListView
from .models import MyModel

class MyModelListView(ListView):
    model = MyModel
    template_name = "myapp/mymodel_list.html"
    context_object_name = "mymodel_list"
    paginate_by = 10
